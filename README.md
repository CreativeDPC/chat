
## Contácto

Daniel Pérez Cabrera    
Cel: 55 1120 0160    
Email:  creativedesing.dpc@gmail.com

## Descripción

Chat Grupal desarrollado con Express y SocketIO


## Instalación

```bash
$ npm install
```

## Ejecución

```bash
# Para Mac y Linux
$ DEBUG=chat:* npm start

# Para Windows
$ set DEBUG=chat:* & npm start


```



## Tecnologías Utilizadas

- [Express Js](https://expressjs.com/es/starter/generator.html/)
- [Socket.IO](https://socket.io/)

## Imagenes

![](https://bitbucket.org/CreativeDPC/chat/raw/c030b113716d342d98e1cae8cc9f677e6202a8fc/screenshots/screen1.png)
![](https://bitbucket.org/CreativeDPC/chat/raw/c030b113716d342d98e1cae8cc9f677e6202a8fc/screenshots/screen2.png)
![](https://bitbucket.org/CreativeDPC/chat/raw/c030b113716d342d98e1cae8cc9f677e6202a8fc/screenshots/screen3.png)