const io = require( "socket.io" )();
const chat = {
    io: io
};

io.on( "connection", function( socket ) {
    console.log( "Conexión exitosa. ID =>", socket.id );
    socket.on("chat:message", (data) =>{
      socket.broadcast.emit("chat:message", data);
    });
});

module.exports = chat;