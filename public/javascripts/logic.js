var socket = io();

let boxChat = document.getElementById("chat-text");
let user = document.getElementById("user-name");
let message = document.getElementById("inp-message");
let btnEnviar = document.getElementById("btn-enviar");

var nombreUser = "";

const sendMessage =() =>{
  if(!message.value) return;

  let _message = `<div class="m-contact message">
                    <div class="pestania"></div>
                    <span>${message.value}</span>
                  </div>`;                  

  boxChat.innerHTML += _message;  
  updateScroll();             

  socket.emit("chat:message", {
  user: nombreUser,
  message: message.value
  });

  message.value = "";
  setTimeout(() => {
  message.focus();
  }, 200);
}

const addMessage = (data) =>{
  let _response = `<div class="m-user message">
                    <div class="pestania"></div>
                    <span><b>${data.user}</b>${data.message}</span>
                  </div>`;

  boxChat.innerHTML += _response; 
  updateScroll();
}

const updateScroll = () =>{
  if(boxChat.scrollHeight)
    boxChat.scroll(0,boxChat.scrollHeight); 
}


btnEnviar.addEventListener("click", sendMessage);

message.addEventListener("keypress", (event) =>{
  const {code} = event;
  if(code === 'Enter'){
    sendMessage();
  }
});

socket.on("chat:message", addMessage);

window.onload = ()=>{
  const getNickName = () =>{
    nombreUser = prompt('Ingresa un Nickname');
    if(!nombreUser){
      getNickName();
    }else{
      user.innerText = nombreUser;
    }
  }
  getNickName();
};